const supertest = require('supertest');
const request = supertest('http://127.0.0.1:3001/');
const path = require('path');



describe('GET Data Elevenia', () => {

    return new Promise( (resolve, reject) => {
        try {

            it('GET dataElevenia/1', async () => {
                request
                    .get('dataElevenia/1')
                    .auth('mumu','Sup3r$3cre+')
                    .expect(200)
                    .end((err,res) => {
                        resolve(res.body);
                        resolve(err)
                        
                    })
            });
        
        } catch (error) {
            reject(error)
        }
    })

    
    
    
});

describe('GET Product one page', () => {

    return new Promise((resolve,reject) => {
        try {

            it('GET jubelio//products/1', async () => {
                request
                    .get('jubelio/products/1')
                    .auth('mumu','Sup3r$3cre+')
                    .expect(200)
                    .end((err,res) => {
                        resolve(err)
                        resolve(res.body)
                    })
            });
            
        } catch (error) {
            reject(error);
        }
    })
    
});

describe('GET Product BY ID', () => {

    return new Promise((resolve,reject) => {
        try {
            it('GET jubelio//product/26682888', async () => {
                request
                    .get('jubelio/product/26682888')
                    .auth('mumu','Sup3r$3cre+')
                    .expect(200)
                    .end((err,res) => {
                        resolve(err);
                        resolve(res.body);
                    })
            });
            
        } catch (error) {
            reject(error)
        }
        
    })
    
});

describe('GET Product Detail', () => {
    return new Promise((resolve,reject) => {
        try {
            it('GET jubelio/productDetail/28022796', () => {
                request
                    .get('jubelio/productDetail/28022796')
                    .auth('mumu','Sup3r$3cre+')
                    .expect(200)
                    .end((err,res) => {
                        resolve(err);
                        resolve(res.body);
                    })
            });
            
        } catch (error) {
            reject(error)
        }
    })
    
});

describe('UPDATE Product BY ID', () => {
    return new Promise( (resolve, reject) => {

        try {

            it('UPDATE jubelio//product/26682888', async () => {
                request
                    .put('jubelio/product/26682888')
                    .field("Content-Type", "multipart/form-data")
                    .field("product_name","product test")
                    .field("price", 120000)
                    .field("description", "Test description")
                    .field("sku","26682888")
                    .attach("image",path.resolve(__dirname,"test.jpg"))
                    .auth('mumu','Sup3r$3cre+')
                    .expect(200)
                    .end((err,res) => {
                        resolve(err);
                        resolve(res.body);
                    })
            });
            
        } catch (error) {
            reject(error)
        }
    })
    
});

describe('INSERT Product', () => {
    return new Promise((resolve,reject) =>{
        try {

            it('POST jubelio//product', () => {
                request
                    .post('jubelio/product')
                    .field("Content-Type", "multipart/form-data")
                    .field("product_name","product test 1234")
                    .field("price", 123400)
                    .field("description", "Test description 1234")
                    .field("sku","12563716_TEST")
                    .attach("image",path.resolve(__dirname,"test.jpg"))
                    .auth('mumu','Sup3r$3cre+')
                    .expect(201)
                    .end((err,res) => {
                        resolve(err);
                        resolve(res.body);
                    })
            });
            
        } catch (error) {
            reject(error)
        }
    })
    
});

describe('DELETE Product BY ID', () => {
    return new Promise((resolve,reject) =>{
        try {

            it('DELETE jubelio/product/26682888', () => {
                request
                    .delete('jubelio/product/26682888')
                    .auth('mumu','Sup3r$3cre+')
                    .expect(200)
                    .end((err,res) => {
                        resolve(err);
                        resolve(res.body);
                    })
            });
            
        } catch (error) {
            reject(error)
        }
    })
    
});
