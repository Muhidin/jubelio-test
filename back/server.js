const Hapi = require('@hapi/hapi');
const Inert = require('@hapi/inert');
const routes = require('./routes/product');

const Bcrypt = require('bcrypt');

const init = async () => {
    const server =  Hapi.server({
        port: 3001,
        host: 'localhost',
        routes: {
            cors: {
                origin: ['*']
            }
        }
    });

 
    const users = {
        mumu: {
            username: 'mumu',
            password: '$2b$10$RoNJhF6hxM2Fvhh9glD0QeRIAxSF1V92XAWovVGsQQyuaFtl3wDGO',   // 'Sup3r$3cre+'
            name: 'Muhidin',
            id: '2133d32a'
        }
    };

    const validate = async (request, username, password) => {

        const user = users[username];
        if (!user) {
            return { credentials: null, isValid: false };
        }
    
        const isValid = await Bcrypt.compare(password, user.password);
        const credentials = { id: user.id, name: user.name };
    
        return { isValid, credentials };
    };


    await server.register(Inert);
    await server.register(require('@hapi/basic'));

    server.auth.strategy('simple', 'basic', { validate });
    server.route(routes);


    await server.start();

    // const salt = await Bcrypt.genSalt(10);
    // // now we set user password to hashed password
    // const password = await Bcrypt.hash('Sup3r$3cre+', salt);

    // console.log(password);

    module.exports = server;

    console.log('Server running on port 3001');
}

init();