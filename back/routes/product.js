const controller = require('../controllers/product');
module.exports = [
    {
        method: "GET",
        path: "/jubelio/products/{page}",
        options: {
            auth: 'simple'
        },
        handler: controller.getProduct
    },

    {
        method: "GET",
        path: "/jubelio/product/{id}",
        handler: controller.getProductByID,
        options: {
            auth: 'simple'
        }

    },

    {
        method: "GET",
        path: "/jubelio/productDetail/{prdNo}",
        handler: controller.getDetailProduct,
        options: {
            auth: 'simple'
        }

    },

    {
        method: "GET",
        path: "/dataElevenia/{page}",
        handler: controller.getEleveniaData,
        options: {
            auth: 'simple'
        }
    },

    {
        method: "GET",
        path: "/jubelio/uploads/{file}",
        handler: {
            directory: {
                path: 'uploads'
            }
        },
        options: {
            auth: 'simple'
        }
    },

    {
        method : "POST",
        path: "/jubelio/product",
        options: {
            payload: {
                output: 'stream',
                parse: true,
                multipart: true,
            },
            auth: 'simple',

            handler: controller.addProduct
        }
    },

    {
        method : "PUT",
        path: "/jubelio/product/{id}",
        options: {
            payload: {
                output: 'stream',
                parse: true,
                multipart: true
            },
            auth: 'simple',

            handler: controller.updateProduct
        }
    },

    {
        method: "DELETE",
        path: "/jubelio/product/{id}",
        handler: controller.deleteProduct,
        options: {
            auth: 'simple',
        }
    },

    {
        method: "GET",
        path: "/jubelio/product/search",
        handler: controller.search,
        options: {
            auth: 'simple',
        }
    }

    


];