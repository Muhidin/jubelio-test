const Boom = require('@hapi/boom');
const model = require('../models/product');

exports.getEleveniaData = async (request, header) => {

    page = request.params.page;
    try {

        const get_data = await model.getDataElevenia(page);
        const get_product = await model.getProduct(page);
        success = 0;
        error = 0;

        for (let i = 0; i < get_data.length; i++) {
            const element = get_data[i];
            const insert = await model.insertProduct(element);

            if(insert == 'error'){
                error ++;
            } else {
                success ++;
            }
        }
        const result = {
            statusCode: 200,
            message: 'Sucess insert data. Total insert : '+success+' , Total Failed : '+error,
            data: get_product
        }

        return header.response(result).code(200);
        
    } catch (error) {
        if(error === 'empty_product'){
            throw Boom.badRequest("Product empty on page "+page)
        }else{
            throw Boom.badImplementation(error);
        }
    }
}

exports.addProduct = async (request, header) => {

    try {
        const handle_image = await model.handleFile(request.payload.image,request.payload.sku)
        const cek_product = await model.cekProduct(request.payload.sku);
        const add_product = await model.addProduct(request.payload, handle_image);
        const getLast = await model.getLastData(request.payload.sku);

        const result = {
            statusCode: 201,
            message: "Insert data success",
            data: getLast
        }

        return header.response(result).code(201);
        
    } catch (error) {

        if(error === 'empty_sku'){
            throw Boom.badRequest("SKU can not be empty ")

        } else if (error === 'sku_exist'){
            throw Boom.badRequest("SKU has been used ")

        } else if (error === 'empty_name'){
            throw Boom.badRequest("Product name can not be empty ")
            
        } else if (error === 'empty_price'){
            throw Boom.badRequest("Price can not be empty ")

        } else if (error === 'empty_image'){
            throw Boom.badRequest("Image can not be empty ")

        } else if (error === 'not_image'){
            throw Boom.badRequest("Please upload correct file (JPG,JPEG,PNG) ")
        }else {
            throw Boom.badImplementation(error.message);
        }
        
    }
}


exports.updateProduct = async (request, header) => {
    try {

        const id = request.params.id;
        const handle_image = await model.handleFile(request.payload.image,id)
        const update_product = await model.updateProduct(id, request.payload, handle_image ? handle_image : '');
        const getLast = await model.getLastData(request.payload.sku);
        const result = {
            statusCode: 200,
            message: "Update data success",
            data: getLast
        }

        return header.response(result).code(200);
        
    } catch (error) {

         if (error === 'empty_name'){
            throw Boom.badRequest("Product name can not be empty ")

        } else if (error === 'empty_price'){
            throw Boom.badRequest("Price can not be empty ")

        } else if (error === 'empty_image'){
            throw Boom.badRequest("Image can not be empty ")

        } else if (error === 'not_image'){
            throw Boom.badRequest("Please upload correct file (JPG,JPEG,PNG) ")
        }


    }
}


exports.getProduct = async (request, header) => {
    const page = request.params.page;
    const size = request.params.size;
    try {
        const getProduct = await model.getProduct(page, size);
        const result = { 
            statusCode: 200, 
            message: 'Success get data product',
            data: getProduct
        }
        return header.response(result).code(200)
    } catch (error) {
        throw Boom.badImplementation(error);
    }
}

exports.getDetailProduct = async (request, header) => {

    const prdNo = request.params.prdNo

    try {
        const cek_product = await model.getProductByID(prdNo);
        const getDetail = await model.getDetailElevenia(prdNo);
        const result = { 
            statusCode: 200, 
            message: 'Success get data detail product',
            data: getDetail
        }
        return header.response(result).code(200)
    } catch (error) {
        if(error === 'empty_product'){
            throw Boom.badRequest("Product not found ")
        }else {
            throw Boom.badImplementation(error);
        }
    }
}

exports.getProductByID = async (request, header) => {
    try {
        const id = request.params.id
        const getProduct = await model.getProductByID(id);
        const result = { 
            statusCode: 200, 
            message: 'Success get data product by ID',
            data: getProduct
        }
        return header.response(result).code(200)
    } catch (error) {
        if(error === "empty_product"){
            throw Boom.badRequest("Product not found")
        }else {
            throw Boom.badImplementation(error);
        }
    }
}

exports.deleteProduct = async (request, header) => {
    try {
        const id = request.params.id
        const delete_data_product = await model.deleteProduct(id);
        const result = { 
            statusCode: 200, 
            message: 'Success delete product'
        }
        return header.response(result).code(200)
    } catch (error) {
        throw Boom.badImplementation(error);
    }
}

exports.search = async (request, header) => {
    try {
        const query = request.query.term
        const data = await model.search(query)

        const result = {
            statusCode: 200,
            message: 'Success search product',
            data: data
        }
        return header.response(result).code(200);
        
    } catch (error) {
        if(error === "empty_product"){
            throw Boom.badRequest("Product not found")
        }else {
            throw Boom.badImplementation(error);
        }
    }
}