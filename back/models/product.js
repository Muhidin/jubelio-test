'use strict';

const { Pool } = require('pg');
const connection = new Pool( {
    user: 'postgres',
    host: '127.0.0.1',
    password: 'pa$$word01',
    database: 'jubelio_testing',
    port: 5432,
    max: 20,
    idleTimeoutMillis: 30000,
    connectionTimeoutMillis: 3000,
});

connection.on('error', (err, client) => {
    console.error('Connection error', err);
    process.exit(-1);
});

const axios = require('axios');
const parsing = require('xml2json');
const fs = require('fs');

exports.getDataElevenia = (page) => {
    return new Promise((resolve, reject) => {
        const config = {
            method: 'get',
            url: 'http://api.elevenia.co.id/rest/prodservices/product/listing?page='+page,
            headers: {
                'openapikey': '721407f393e84a28593374cc2b347a98'
            },
            responseType: 'xml',
            responseEncoding: 'utf8',
            xsrfCookieName: 'XSRF-TOKEN',
            xsrfHeaderName: 'X-XSRF-TOKEN',
            timeout: 600000
        };
        axios(config)
        .then((response) => {
            const json_convert = parsing.toJson(response.data);
            const product = JSON.parse(json_convert).Products.product;

            if(product === undefined){
                reject("empty_product");
            }else {
                resolve(product);
            }
            
        })
        .catch(function (error) {
            if (error.response) {
                reject(error.message);
            } else if (error.request) {
                reject(error.message);
            } else {
                console.log('Error', error.message);
                reject(error.message);
            }
        })
    });
}

exports.getDetailElevenia = (product_id) => {
    return new Promise((resolve, reject) => {
        const config = {
            method: 'get',
            url: 'http://api.elevenia.co.id/rest/prodservices/product/details/'+product_id,
            headers: {
                'openapikey': '721407f393e84a28593374cc2b347a98'
            },
            responseType: 'xml',
            responseEncoding: 'utf8',
            xsrfCookieName: 'XSRF-TOKEN',
            xsrfHeaderName: 'X-XSRF-TOKEN',
            timeout: 600000
        };
        axios(config)
        .then((response) => {
            
            const json_convert = parsing.toJson(response.data);
            const product = JSON.parse(json_convert).Product;

            if(product){
                resolve(product);
            }else {
                const result = JSON.parse(json_convert);
                if(result.ClientMessage.resultCode === '200'){
                    reject('empty_product');
                }
            }
            
        })
        .catch(function (error) {
            if (error.response) {
                reject(error.message);
            } else if (error.request) {
                reject(error.message);
            } else {
                console.log('Error', error.message);
                reject(error.message);
            }
        })
    });
}


exports.insertProduct = (product) => {
    return new Promise( (resolve, reject) => {
        const query = `INSERT INTO "public"."tbl_product" 
            (
                "prdNo", 
                "prdimage", 
                "prdNm", 
                "bndlDlvCnYn", 
                "cuponcheck", 
                "dispCtgrNo", 
                "dispCtgrStatCd", 
                "exchDlvCst", 
                "imageKindChk", 
                "optionAllAddPrc", 
                "outsideYnIn", 
                "outsideYnOut", 
                "prdSelQty", 
                "prdUpdYN", 
                "preSelPrc", 
                "proxyYn", 
                "rtngdDlvCst", 
                "saleEndDate", 
                "saleStartDate", 
                "selMthdCd", 
                "selPrc", 
                "selStatCd", 
                "selTermUseYn", 
                "sellerItemEventYn", 
                "sellerPrdCd", 
                "shopNo", 
                "tmpltSeq", 
                "nResult", 
                "dispCtgrNm", 
                "dispCtgrNmMid", 
                "dispCtgrNmRoot", 
                "dscAmt", 
                "dscPrice", 
                "freeDelivery", 
                "dispCtgrNo1", 
                "stock", 
                "prd_desc"
            ) 
            VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,$31,$32,$33,$34,$35,$36,$37) ON CONFLICT ("prdNo") DO NOTHING;
        `;

        const values = [
            product.prdNo,
            'https://media-exp1.licdn.com/dms/image/C560BAQEFjr2BaNGHaQ/company-logo_200_200/0/1643013091125?e=2147483647&v=beta&t=YdITpGaJDdENsDNIhG8TD3T4Q-jRsFymcIGBIeOaegM',
            product.prdNm,
            product.bndlDlvCnYn,
            product.cuponcheck,
            product.dispCtgrNo,
            product.dispCtgrStatCd, 
            product.exchDlvCst, 
            product.imageKindChk, 
            product.optionAllAddPrc, 
            product.outsideYnIn, 
            product.outsideYnOut, 
            product.prdSelQty, 
            product.prdUpdYN, 
            product.preSelPrc,
            product.proxyYn, 
            product.rtngdDlvCst, 
            product.saleEndDate, 
            product.saleStartDate, 
            product.selMthdCd, 
            parseInt(product.selPrc), 
            product.selStatCd, 
            product.selTermUseYn, 
            product.sellerItemEventYn, 
            product.sellerPrdCd, 
            product.shopNo, 
            product.tmpltSeq, 
            product.nResult, 
            product.dispCtgrNm, 
            product.dispCtgrNmMid, 
            product.dispCtgrNmRoot, 
            product.dscAmt, 
            product.dscPrice, 
            product.freeDelivery, 
            product.dispCtgrNo1, 
            parseInt(product.stock), 
            product.prd_desc

        ]

        connection.connect( (err, client, done) => {
            if(err) {
                reject(err.message);
            } else {
                client.query(query, values, (err, res) => {
                    done();
                    if(err){
                        reject(err.message);
                    } else {
                        resolve('success');
                    }
                })
            }
        })
    });
}


exports.addProduct = (product, image) => {

    return new Promise( (resolve, reject) => {
        if(product.sku === ""){
            reject('empty_sku')
        }

        if(product.product_name === ""){
            reject('empty_name')
        }

        if(product.price === ""){
            reject('empty_price')
        }

        if(product.image === ""){
            reject('empty_image')
        }

        const query =` INSERT INTO "public"."tbl_product" ("prdNo", "prdNm","selPrc","prdimage","prd_desc")
                    VALUES ($1,$2,$3,$4,$5);
        `;

        const values = [product.sku, product.product_name,product.price,image,product.description]

        connection.connect( (err, client, done) => {
            if(err) {
                reject(err.message);
            } else {
                done();

                client.query(query, values, (err, res) => {

                    if (err){
                        reject(err.message);
                    } else {
                        resolve(true);
                    }
                })
            }
        })
    })

}

exports.cekProduct = (sku) => {
    return new Promise((resolve, reject) => {
        
        if (sku === ""){
            reject("empty_sku");
        }else {
            const query = `SELECT "prdNo" FROM tbl_product WHERE "prdNo" = $1 `;
            const values = [sku];

            connection.connect( (err, client, done) => {
                if(err){
                    reject(err.message);
                } else {
                    done();
                    client.query(query, values, (err, res) => {
                        if (err) {
                            reject(err.message);
                        } else {
                            if (res.rowCount > 0) {
                                reject('sku_exist');
                            }else {
                                resolve(true);
                            }
                        }
                    })
                }
            })
        }
    });
}

exports.getLastData = (sku) => {
    return new Promise( (resolve, reject) => {
        if (sku === ""){
            reject("empty_sku");
        }else {
            const query = `SELECT "id","prdNo","prdNm","selPrc","prdimage","prd_desc" FROM tbl_product WHERE "prdNo" = $1 `;
            const values = [sku];

            connection.connect( (err, client, done) => {
                if(err){
                    reject(err.message);
                } else {
                    done();
                    client.query(query, values, (err, res) => {
                        if (err) {
                            reject(err.message);
                        } else {
                           resolve(res.rows)
                        }
                    })
                }
            })
        }
    })
}


exports.getProduct = (pageNumber) => {

    const size = 25;

    return new Promise((resolve, reject) => {
        
            const query = `SELECT "id","prdNo","prdNm","selPrc","prdimage","prd_desc","dispCtgrNm" FROM tbl_product ORDER BY id DESC LIMIT $2 OFFSET (($1 - 1) * $2)`;
            const values = [pageNumber,size]

            connection.connect( (err, client, done) => {
                if(err){
                    reject(err.message);
                } else {
                    done();
                    client.query(query, values, (err, res) => {
                        if (err) {
                            reject(err.message);
                        } else {
                            resolve(res.rows);
                        }
                    })
                }
            })

    });
    
}

exports.getProductByID = (sku) => {
    
    return new Promise((resolve, reject) => {
        
        const query = `SELECT "prdNo","prdNm","selPrc","prdimage","prd_desc","dispCtgrNm" FROM tbl_product WHERE "prdNo" = $1`;
        const values = [sku];

        connection.connect( (err, client, done) => {
            if(err){
                reject(err.message);
            } else {
                done();
                client.query(query, values,(err, res) => {
                    if (err) {
                        reject(err.message);
                    } else {
                        if(res.rowCount > 0) {
                            resolve(res.rows);
                        }else {
                            reject('empty_product');
                        }
                    }
                })
            }
        })

    });
}

exports.updateProduct = (id,product,image) => {

    return new Promise( (resolve, reject) => {

        if(product.product_name === ""){
            reject('empty_name')
        }

        if(product.price === ""){
            reject('empty_price')
        }

        const query =`UPDATE tbl_product  SET "prdNm" = $1, "selPrc" = $2, "prdimage" = $3, "prd_desc" = $4 WHERE "prdNo" = $5`;

        const values = [product.product_name,product.price,image,product.description,id]

        connection.connect( (err, client, done) => {

            if(err) {
                reject(err.message);
            } else {
                done();

                client.query(query, values, (err, res) => {

                    if (err){
                        reject(err.message);
                    } else {
                        resolve(true);
                    }
                })
            }
        })
        
    })
    
}

exports.deleteProduct = (id) => {
    
    return new Promise((resolve, reject) => {
        
        const query = `DELETE FROM tbl_product WHERE "prdNo" = $1`;
        const values = [id]

        connection.connect( (err, client, done) => {
            if(err){
                reject(err.message);
            } else {
                done();
                client.query(query,values, (err, res) => {
                    if (err) {
                        reject(err.message);
                    } else {
                        resolve(true);
                    }
                })
            }
        })

    });
}

exports.handleFile = (file,sku) => {
    return new Promise( (resolve, reject) => {
        const filename = sku;
        const data = file._data;
        const ext = file.hapi.filename.split('.').pop();

        if(file.hapi.filename === undefined || file.hapi.filename === 'undefined' || file.hapi.filename === ''){
            reject('empty_image');
        }

        if (ext === 'jpg' || ext === 'jpeg' || ext === 'JPG' || ext === 'JPEG' || ext === 'PNG') {
           // resolve(true);
           return fs.writeFile(`./uploads/${filename}.${ext}`, data, err => {

                if (err) {
                    return reject(err)
                }

                return resolve(`http://localhost:3001/jubelio/uploads/${filename}.${ext}`)
            })
        } else {
            reject('not_image');
        }

    })
}



