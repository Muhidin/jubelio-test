import { makeAutoObservable } from 'mobx'
import { useContext, createContext } from 'react';

class RootStore {

    product = [];
    product_detail =[];


    page = 1;
    items = 25;
    hasMore = true;
    sku  = '';
    name = '';
    price = '';
    image = '';
    description = '';
    search = '';
    filter_product = '';
    filter_price = '';
    show = false;

    constructor(){
        makeAutoObservable(this);
    }

    pushProduct(e){
        this.product.push(e);
    }

    updateProduct(product,product_id){
        for (let i = 0; i < this.product.length; i++) {
            const id = this.product[i].id;
            if (parseInt(id) === parseInt(product_id)) {
                this.product[i] = {
                    ...product
                }
            }
        }
    }

    removeProduct(index){
        this.product.splice(index,1);
    }

    addPage(){
        this.page++;
    }

    setSku (sku) {
        this.sku = sku;
    }

    setName (name) {
        this.name = name;
    }

    setPrice (price) {
        this.price = price;
    }

    setDescription (description) {
        this.description = description;
    }

    setImage (image) {
        this.image = image;
    }

    setSearch(event){
        this.search = event;
    }

    setFilterProduct(event){
        this.filter_product = event
    }

    setFilterPrice(event){
        this.filter_price = event;
    }

    setDetail(event){
        this.product_detail = event;
    }

    setShowModal(event){
        this.showModal = event;
    }

    setProduct(event){
        this.product = event;
    }


    spliceProduct(e) {
        for (let i = 0; i < this.product.length; i++) {
            const id = this.product[i].id;
            if (parseInt(id) === parseInt(e)) {
                this.product.splice(i, 1);
            }
        }
    }


    getAllProduct(){

    }

    filterDetail(prdNo){
        this.product_detail.findIndex(function(o){
            return o.prdNo === prdNo
        })
    }




}

const store = new RootStore();
const StoreContext = createContext(store);

function useRootStore() {
    
    const context = useContext(StoreContext);

    return context;
}

function RootStoreProvider({ children }) {
    //only create the store once ( store is a singleton)
    const root = store ?? new RootStore()
    
    return <StoreContext.Provider value={root}>{children}</StoreContext.Provider>
  }


export {
    store,
    useRootStore,
    RootStoreProvider,
}