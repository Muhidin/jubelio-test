import './App.css';
import { observer } from 'mobx-react';
import 'bootstrap/dist/css/bootstrap.min.css';
import ProductListComponent from './components/ProductList.js';
import { Routes, Route } from "react-router-dom";
import Productdetail from './components/ProductDetail';

function App() {
  return (
    <div className="App">
      
          <Routes>
            <Route path="/" element={<ProductListComponent />} />
            <Route path="product/detail/:id" element={<Productdetail />} />
          </Routes>
      
      
    </div>
  );
}

export default observer(App);
