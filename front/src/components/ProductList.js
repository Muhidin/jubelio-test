import React, { useEffect, useState } from "react";
import { observer } from "mobx-react";
import { useRootStore } from "../store/RootStore";
import axios from 'axios';
import InfiniteScroll from "react-infinite-scroll-component";
import {Button,Card,Row,Col, Container} from 'react-bootstrap'


import ModalAdd from "./ModalAdd";
import Product from "./Product.js";

function ProductList() {

    const [modalAdd, setModalAdd] = useState({show: false,id: null,sku: '',product_name: '',price: '',description: '',image: ''})

    const store = useRootStore();


    const getdataElevenia = async () => {

      return new Promise( (resolve, reject) => {
          const config = {
              method: 'get',
              url: 'http://localhost:3001/dataElevenia/'+store.page,
              headers: { 
                'Authorization': 'Basic bXVtdTpTdXAzciQzY3JlKw=='
            },
            responseEncoding: 'utf8',
            xsrfCookieName: 'XSRF-TOKEN',
            xsrfHeaderName: 'X-XSRF-TOKEN',
            timeout: 600000
          };
  
          axios(config)
              .then(function (response) {

                let data = response.data.data;
                for (let i = 0; i < data.length; i++) {
                    const e = data[i];
                    store.pushProduct(e);
                    
                }
                store.addPage();
                  
                 resolve(response.message)
          })
          .catch(function (error) {
              if (error.response) {
                  reject(error.message);
              } else if (error.request) {
                  reject(error.message);
              } else {
                  reject(error.message);
              }
          });
      })

  }



    useEffect( () => {
        getdataElevenia();

    }, []);



    return (
      <div>

        <Container className="container-fluid">

        <br />
        <Row>
            <Col md={6}>
                <h3>Product List</h3>
                <Button variant="primary" onClick={() => setModalAdd({show:true,sku:'', product_name:'',price:'',description:'',image:''})}>Add New</Button>
            </Col>

            <Col md={6}>
                &nbsp;
            </Col>
        </Row>

        <br />
        {/* <Row>
            <Filter product={store.product} />
        </Row> */}

        {/* <ModalDelete show={modalDelete.show} onHide={() => setModalDelete({show: false}) } data={modalDelete} /> */}
        <ModalAdd show={modalAdd.show}  onHide={() => setModalAdd({show: false})} data={modalAdd} />
        {/* <ModalEdit show={modalEdit.show} onHide={() => setModalEdit({show: false})} data={modalEdit}/> */}

        <InfiniteScroll 
          dataLength={store.product.length} //This is important field to render the next data
          next={getdataElevenia}
          hasMore={store.hasMore}
          loader={<h4></h4>}
        >
            

            <Product product={store.product} />


        </InfiniteScroll>

        </Container>

        </div>

    )
    
}

export default observer(ProductList);