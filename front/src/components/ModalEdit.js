import React, { useEffect, useState } from "react";
import { Modal,Button,Form,Col } from 'react-bootstrap';
import axios from 'axios';
import { store, useRootStore } from "../store/RootStore";

function ModalEdit(props) {

    const store  = useRootStore();

    const [inputs,setInputs] = useState({
        id: props.data.id,
        sku: props.data.sku,
        product_name: props.data.product_name,
        price: props.data.price,
        description: props.data.description,
        image: props.data.image
    })
    useEffect(() => {
        const updateInputs = () => {
            setInputs(props.data)
        }
        updateInputs()
    })
    const changeForm = (event) => {
        event.persist();
        let state = inputs;
        if (event.target.id === "file") {
            state[event.target.id] = event.target.files[0];
        } else {
            state[event.target.id] = event.target.value;
        }
        setInputs({state})
    }
    const onSubmitEdit = (event) => {
        event.preventDefault();
        let data = new FormData();
        data.append('sku', inputs.sku);
        data.append('product_name', inputs.product_name);
        data.append('price', inputs.price);
        data.append('description', inputs.description);
        var imagefile = document.querySelector('#file');
        data.append("image", imagefile.files[0]);

        const config = {
            method: 'put',
            url: 'http://127.0.0.1:3001/jubelio/product/'+inputs.sku,
            headers: {
              'Content-Type': 'multipart/form-data',
              'Authorization': 'Basic bXVtdTpTdXAzciQzY3JlKw=='
            },
            data : data
 
        };
        axios(config)
        .then((response) => {
            alert(response.data.message);
            setInputs({
                id:'',
                sku:'',
                product_name:'',
                price:'',
                description:'',
                image:''
            })

            const data = response.data.data[0];
            store.updateProduct(data, data.id)
            props.onHide();
        })
        .catch(function (error) {
            console.log(error);
            if (error.response) {
                alert(error.response.data.message);
            } else if (error.request) {
                alert(error.request);
            } else {
                alert(error.message);
            }
        })
    }

    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Edit Product
          </Modal.Title>
        </Modal.Header>
        <Form onSubmit={onSubmitEdit}>
        <Modal.Body>
            <Form.Group>
                <Form.Label htmlFor="sku">SKU :</Form.Label>
                <Col sm={12}>
                    <Form.Control type="text" id="sku" name="sku" placeholder="SKU" required value={inputs.sku || ''} onChange={changeForm} readOnly/>
                </Col>
            </Form.Group>
            <br />

            <Form.Group>
                <Form.Label htmlFor="product_name">Product Name :</Form.Label>
                <Col sm={12}>
                    <Form.Control type="text" id="product_name" placeholder="Product Name" required value={inputs.product_name || ''} onChange={changeForm} />
                </Col>
            </Form.Group>

            <Form.Group>
                <Form.Label htmlFor="price">Price :</Form.Label>
                <Col sm={12}>
                    <Form.Control type="number" id="price" placeholder="Price"  value={inputs.price || ''} onChange={changeForm} required />
                </Col>
            </Form.Group>

            <Form.Group>
                <Form.Label htmlFor="desc">Description :</Form.Label>
                <Col sm={12}>
                    <textarea id="description" className="form-control" value={inputs.description || ''} onChange={changeForm} ></textarea>
                </Col>
            </Form.Group>

            <Form.Group>
                <Form.Label htmlFor="file">Image :</Form.Label>
                <Col sm={12}>
                    <Form.Control type="file" id="file" placeholder="Image" required />
                </Col>
            </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Close</Button>
          <Button type="submit">Save</Button>
        </Modal.Footer>

        </Form>
      </Modal>
    );
  }

  export default ModalEdit;