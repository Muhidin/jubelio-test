import React, {useState} from 'react';
import { Card, Row, Col, Button, Container,Form } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import ModalDelete from './ModalDelete';
import { useRootStore } from '../store/RootStore';
import ModalEdit from './ModalEdit';
const Product = (props) => {

    const store = useRootStore();

    const [modalDelete, setModalDelete] = useState({show: false,id: null,sku: '',product_name: '',price: '',description: '',image: ''})
    const [modalEdit, setModalEdit] = useState({show: false,id: null,sku: '',product_name: '',price: '',description: '',image: ''})

    const dataList = store.product
    const [data, setData] = useState(dataList);
    const excludeSearch = ['id', 'prdNo', 'prdimage', 'prd_desc', 'dispCtgrNm'];

    const onSearchChange = (e) => {
        store.setSearch(e);

        const lowercasedValue = e.toLowerCase().trim();
        if (lowercasedValue === "") setData(dataList);
        else {
          const filteredData = dataList.filter(item => {
            return Object.keys(item).some(key =>
              excludeSearch.includes(key) ? false : item[key].toString().toLowerCase().includes(lowercasedValue)
            );
          });
          setData(filteredData);
        }
    }

    const onPriceChange = (e) => {
        store.setPrice(e)
        const lowercasedValue = e.toLowerCase().trim();
        if (lowercasedValue === "") setData(dataList);
        else {
          const filteredData = dataList.filter(item => {
            return Object.keys(item).some(key =>
              excludeSearch.includes(key) ? false : item[key].toString().toLowerCase() === lowercasedValue
            );
          });
          setData(filteredData);
        }
    }

    const onProductChange = (e) => {
        const lowercasedValue = e.toLowerCase().trim();
        if (lowercasedValue === "") setData(dataList);
        else {
          const filteredData = dataList.filter(item => {
            return Object.keys(item).some(key =>
              excludeSearch.includes(key) ? false : item[key].toString().toLowerCase().includes(lowercasedValue)
            );
          });
          setData(filteredData);
        }
    }

    return (
        <div>
            <Container fluid>
                <ModalEdit show={modalEdit.show} onHide={() => setModalEdit({show: false})} data={modalEdit}/>
                <ModalDelete show={modalDelete.show} onHide={() => setModalDelete({show: false}) } data={modalDelete} />
    

                <Row>
                    <Col md={4}>
                        &nbsp;
                    </Col>
                    <Col md={4}>
                        <span>Filter price</span>
                    </Col>

                    <Col md={4}>
                        <span>Filter Product</span>
                    </Col>
                </Row>

                <Row>
                    <Col md={4}>
                        <input type="text" className='form-control' onChange={(e) => onSearchChange(e.target.value)} placeholder="type to search..."/>
                    </Col>
                    <Col md={4}>
                        <Form.Select onChange={(e) => onPriceChange(e.target.value)}>
                            <option value="">All</option>
                            {props.product.map((e,i) => {
                                return <option key={i} value={e.selPrc}>{e.selPrc}</option>
                            })}
                        </Form.Select>
                    </Col>

                    <Col md={4}>
                        <Form.Select onChange={(e) => onProductChange(e.target.value)}>
                            <option value="">All</option>
                            {props.product.map((e,i) => {
                                return <option key={i} value={e.prdNm}>{e.prdNm}</option>
                            })}
                        </Form.Select>
                    </Col>
                </Row>
                <Row>
                    { data.map( (e, i) => {
                        return(
                            <Col key={i} md={4} >
                                <div>
                                <br />
                                <Card className='p-2' style={{ maxWidth: 345 }} >

                                    <Card.Body>

                                    <Link to={'/product/detail/' + e.prdNo}>
                                        <Card.Img variant="top" src={e.prdimage} style={{width:'100 px'}}/>
                                    </Link>
                                    

                                    <center> 
                                    <Link to={'/product/detail/' + e.prdNo}><Card.Subtitle className="mb-2 text-muted">{e.prdNo}</Card.Subtitle><Card.Text><strong>{e.prdNm}</strong></Card.Text> </Link>
                                    <Card.Text>Price Rp. {e.selPrc}</Card.Text>
                                    <Card.Text>
                                        {e.prd_desc}
                                    </Card.Text>

                                    <Button variant="primary" onClick={() => {setModalEdit({
                                        show: true, sku: e.prdNo, product_name: e.prdNm, price: e.selPrc, description: e.prd_desc, image: e.prdimage
                                    })}}>Edit</Button>

                                    &nbsp;&nbsp;
                                    <Button variant="danger" onClick={() => {
                                        setModalDelete({show: true,id: e.id,sku: e.prdNo,product_name: e.prdNm,price: e.selPrc,description: e.prd_desc, image: e.prdimage})}
                                    }>Delete</Button>
                                    </center>
                                
                                    </Card.Body>
                                </Card>
                                </div>
                                <br />

                                

                            </Col>
                            
                        )
                    })}

                    {data.length === 0 && <span>No records found</span>}

                </Row>
            </Container>
        </div>
    );
}

export default Product;
