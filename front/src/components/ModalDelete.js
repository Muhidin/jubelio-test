import React from "react";
import axios from 'axios';
import { Modal, Button, Form } from 'react-bootstrap';
import { useRootStore } from "../store/RootStore";


function ModalDelete(props) {

  const store  = useRootStore();

    const handleSubmit = (event) => {
        event.preventDefault();

        var config = {
            method: 'delete',
            url: 'http://localhost:3001/jubelio/product/'+props.data.sku,
            headers: { 
              'Authorization': 'Basic bXVtdTpTdXAzciQzY3JlKw=='
            }
          };
          
        axios(config)
        .then((response) => {
            alert(response.data.message);
            store.spliceProduct(props.data.id);
            props.onHide();
            //window.location.reload();
        })
        .catch(function (error) {
            console.log(error);
            if (error.response) {
                alert(error.response.data.message);
            } else if (error.request) {
                console.log(error.request);
                alert(error.request);
            } else {
                console.log('Error', error.message);
                alert(error.message);
            }
        })

    }
    
    return (
        <Modal
          {...props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Delete Product
            </Modal.Title>
          </Modal.Header>
          <Form>
          <Modal.Body>
              <span>Delete Product <span className="text-danger"> <b>{props.data.product_name} ? </b> </span></span>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={props.onHide}>Close</Button>
            <Button onClick={handleSubmit} type="submit">Delete</Button>
          </Modal.Footer>
          </Form>
        </Modal>
    )
}

export default ModalDelete