import React,{useState,useEffect} from 'react';
import { Card, Row,Col } from 'react-bootstrap';
import { Link,useParams } from 'react-router-dom';
import { useRootStore } from '../store/RootStore';
import axios from 'axios';

const Productdetail = () => {
    const {id} = useParams();
    const store = useRootStore();

      const [products, setProducts] = useState([]);
      useEffect(() => {
        fetchDetail();
      }, []);

      const fetchDetail = () => {
        const config = {
            method: 'get',
            url: 'http://localhost:3001/jubelio/productDetail/'+id,
            headers: { 
              'Authorization': 'Basic bXVtdTpTdXAzciQzY3JlKw=='
            },
            responseEncoding: 'utf8',
            xsrfCookieName: 'XSRF-TOKEN',
            xsrfHeaderName: 'X-XSRF-TOKEN',
            timeout: 600000
          };
          
          axios(config)
          .then(function (response) {
            setProducts(response.data.data)
          })
          .catch(function (error) {
            console.log(error);
          });
      }

    return (

        <div className='container-fluid'>
            <br /><br />
   
            <Link to="/">Back</Link>

                    <Row  >
                        <Col md={6}>
                            <Row>
                                <Col md={12}>
        
                                    <Card.Img className='BeerListItem-img' variant="left"  src={products.prdImage01}  style={{width:'100 px',height:'100 px'}}/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4}>
                                    <img src={products.prdImage02} alt={products.prdNm} className="img-thumbnail" />
                                </Col>
                                <Col md={4}>
                                    <img src={products.prdImage03} alt={products.prdNm} className="img-thumbnail" />
                                </Col>
                                <Col md={4}>
                                    <img src={products.prdImage04} alt={products.prdNm} className="img-thumbnail" />
                                </Col>
                            
                                
                            </Row>
                        
                        </Col>
                    
                        <Col md={6}>
                            <Card.Title>
                                <Card.Text>{products.prdNm}</Card.Text>
                            </Card.Title>
                                <Card.Text>
                                    Code : {products.sellerPrdCd}
                                </Card.Text>
                                <Card.Text>
                                    Price : Rp. {products.selPrc}
                                </Card.Text>
                                <Card.Text>Description : <br />
                                    <div dangerouslySetInnerHTML={{__html: products.htmlDetail}} />
                                </Card.Text>

                           
                        </Col>
                    </Row> 



        </div>
    );
}

export default Productdetail;
