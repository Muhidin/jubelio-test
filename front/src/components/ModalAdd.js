import React, { useEffect, useState } from "react";
import { Modal,Button,Form,Col, Alert } from 'react-bootstrap';
import axios from 'axios';
import { useRootStore } from "../store/RootStore";


function ModalAdd(props) {

    const store  = useRootStore();

    useEffect(() => {
        const clean = () => {
            store.setSku('');
            store.setName('')
            store.setPrice('');
            store.setDescription('')
            store.setImage('');
        }

        clean();
    });


    const handleSubmit = (event) => {
        event.preventDefault();
        let data = new FormData();
        data.append('sku', store.sku);
        data.append('product_name', store.name);
        data.append('price', store.price);
        data.append('description', store.description);
        var imagefile = document.querySelector('#file');
        data.append("image", imagefile.files[0]);


        const config = {
            method: 'post',
            url: 'http://localhost:3001/jubelio/product',
            headers: { 
              'Authorization': 'Basic bXVtdTpTdXAzciQzY3JlKw==', 
              'Content-Type': 'multipart/form-data',
            },
            data : data,
          };
          
        axios(config)
        .then((response) => {
            store.pushProduct(response.data.data[0]);
            alert(response.data.message);
            props.onHide();
        })
        .catch(function (error) {
            console.log(error);
            if (error.response) {
                alert(error.response.data.message);
            } else if (error.request) {
                console.log(error.request);
                alert(error.request);
            } else {
                console.log('Error', error.message);
                alert(error.message);
            }
        })

    }

    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Add Product
          </Modal.Title>
        </Modal.Header>
        <Form onSubmit={handleSubmit}>
        <Modal.Body>
            <Form.Group>
                <Form.Label htmlFor="sku">SKU :</Form.Label>
                <Col sm={12}>
                    <Form.Control type="text" name="sku" id="sku" placeholder="SKU" onChange={(e) => {store.setSku(e.target.value)}} required />
                </Col>
            </Form.Group>
            <br />

            <Form.Group>
                <Form.Label htmlFor="product_name">Product Name :</Form.Label>
                <Col sm={12}>
                    <Form.Control type="text" name="product_name" id="product_name" placeholder="Product Name" onChange={(e) => {store.setName(e.target.value)}} required/>
                </Col>
            </Form.Group>

            <Form.Group>
                <Form.Label htmlFor="price">Price :</Form.Label>
                <Col sm={12}>
                    <Form.Control type="number" name="price" id="price" placeholder="Price"  onChange={(e) => {store.setPrice(e.target.value)}} required />
                </Col>
            </Form.Group>

            <Form.Group>
                <Form.Label htmlFor="description">Description :</Form.Label>
                <Col sm={12}>
                    <textarea id="description" name="description" className="form-control" onChange={(e) => {store.setDescription(e.target.value)}}></textarea>
                </Col>
            </Form.Group>

            <Form.Group>
                <Form.Label htmlFor="file">Image :</Form.Label>
                <Col sm={12}>
                    <Form.Control type="file"  name="file" id="file" accept="image/*" required />
                </Col>
                
            </Form.Group>

        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Close</Button>
          <Button type="submit">Save</Button>
        </Modal.Footer>
        </Form>
      </Modal>
    );
  }

  export default ModalAdd;